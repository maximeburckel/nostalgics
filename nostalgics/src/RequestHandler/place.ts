import { prisma } from "../db";
import express, { Request, Response } from 'express'
import { HttpError } from "../error";



export async function get_all(req: Request, res: Response) {
    const places = await prisma.place.findMany();
    places? res.json(places) : res.send("Places not found")
};

export async function get_one(req: Request, res: Response) {
    const place = await prisma.place.findFirst({
        where : {
            id : Number(req.params.place_id)
        }
    });

    place? res.json(place) : res.send("Place not found")
};

export async function create_one(req: Request, res: Response) {
    await prisma.place.create({
        data: req.body
    })
    res.status(201).send("Place successfully added")
    
};

export async function update_one(req: Request, res: Response) {
    try{
        await prisma.place.update({
            where : {
                id : parseInt(req.params.place_id)
            },
            data : req.body
        })
    }
    catch(error){
        throw new HttpError("Error during update", 400)
    }
    
    
    res.status(200).send("Place updated")
       
    
   
};

export async function delete_one(req: Request, res: Response) {
    try{
    await prisma.place.delete({
        where : {
                id : parseInt(req.params.place_id)
        }
    })
    }catch(error){
        throw new HttpError("Error during delete", 400)
    }
    

    res.status(204).send("Place successfully deleted")
};
  