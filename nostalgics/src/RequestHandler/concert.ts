import { prisma } from "../db";
import express, { Request, Response } from 'express'
import { HttpError } from "../error";
import { format } from 'date-fns';
import { fr } from 'date-fns/locale';




export async function get_all(req: Request, res: Response) {
    const concerts = await prisma.concert.findMany({
        include : {
        place : true
    }
    });
    concerts? res.json(concerts) : res.send("Concert not found")
};

export async function get_one(req: Request, res: Response) {
    const concerts = await prisma.concert.findFirst({
        where : {
            id : Number(req.params.concert_id)
        },
        include : {
            place : true
        }
    });

    concerts? res.json(concerts) : res.send("Concert not found")
};

export async function create_one(req: Request, res: Response) {
    const { placeId, comment, date } = req.body;

    try {
      const newConcert = await prisma.concert.create({
        data: {
            placeId,
            comment,
            date ,
        },
      });
  
      res.status(201).json(newConcert);
    } catch (error) {
      res.status(500).json("Error during creation");
    }
  }

export async function update_one(req: Request, res: Response) {
    try{
        await prisma.concert.update({
            where : {
                id : parseInt(req.params.concert_id)
            },
            data : req.body
        })
    }
    catch(error){
        throw new HttpError("Error during update", 400)
    }
    
    
    res.status(200).send("Concerts updated")
       
    
   
};

export async function delete_one(req: Request, res: Response) {
    try{
    await prisma.concert.delete({
        where : {
                id : parseInt(req.params.concert_id)
        }
    })
    }catch(error){
        throw new HttpError("Error during delete", 400)
    }
    

    res.status(204).send("Concert successfully deleted")
};
  