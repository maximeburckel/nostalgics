import express, { Request, Response, NextFunction } from 'express';
import { StructError } from 'superstruct';
import { HttpError } from './error';
import * as concert from './RequestHandler/concert';
import * as place from './RequestHandler/place';

const app = express();
const port = 3000;

app.use(express.json())

app.get('/', (req: Request, res: Response) => {
  res.send('Welcome new member!');
});


//Concert requests
app.get('/concerts', concert.get_all)

app.get('/concerts/:concert_id', concert.get_one)

app.post('/concerts', concert.create_one)

app.patch('/concerts/:concert_id', concert.update_one)

app.delete('/concerts/:concert_id', concert.delete_one)


//Place requests
app.get('/places', place.get_all)

app.get('/places/:place_id', place.get_one)

app.post('/places', place.create_one)

app.patch('/places/:place_id', place.update_one)

app.delete('/places/:place_id', place.delete_one)

app.use((err: HttpError, req: Request, res: Response, next: NextFunction) => {
  if (err instanceof StructError) {
    err.status = 400;
    err.message = String(`Bad value for field ${err.key}`)
  }

  res.status(err.status ?? 500).send(err.message);
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
