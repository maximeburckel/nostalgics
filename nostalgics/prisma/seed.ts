import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

async function main() {
    const place = await prisma.place.create({
        data: {
          city: 'Sample City',
          postalCode: 12345,
          country: 'Sample Country',
        },
      });
      await prisma.concert.create({
        data: {
          placeId: place.id,
          comment: 'Sample Concert Comment',
          date: new Date(), // You can set a specific date here
        },
      });
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
